Description: Add missing files not included in the PyPI tarballs.
Origin: https://github.com/tilezen/mapbox-vector-tile/tree/0.5.0

--- /dev/null
+++ b/CHANGELOG.md
@@ -0,0 +1,109 @@
+Version 0.5.0
+-------------
+
+* Improved results from `on_invalid_geometry_make_valid` when the geometry is self-crossing. It was possible for large parts of the geometry to be discarded, and it is now less likely. See [PR 66](https://github.com/tilezen/mapbox-vector-tile/pull/66) for more information.
+
+Version 0.4.0
+-------------
+
+* Custom rounding functions: a `round_fn` parameter was added to the `encode` function, which allows control over how floating point coordinates are transformed to integer ones. See [PR 55](https://github.com/tilezen/mapbox-vector-tile/pull/55).
+* Custom validity functions: an `on_invalid_geometry` parameter was added to the `encode` function, which is called when invalid geometry is found, or created through coordinate rounding. See [PR 46](https://github.com/tilezen/mapbox-vector-tile/pull/46).
+* Winding order bug fix: See [issue 57](https://github.com/tilezen/mapbox-vector-tile/issues/57) and [PR 59](https://github.com/tilezen/mapbox-vector-tile/pull/59).
+* Performance improvements: including a 2x speedup from using `tuple`s instead of `dict`s for coordinates, see [PR 56](https://github.com/tilezen/mapbox-vector-tile/pull/56).
+* Improvements to PY3 compatibility: See [PR 52](https://github.com/tilezen/mapbox-vector-tile/pull/52).
+
+Version 0.3.0
+-------------
+
+* python3 compatability improvements
+* travis integration
+* documentation updates
+* insert CMD_SEG_END for MultiPolygons
+* decode multipolygons correctly
+* encode tiles using version 1
+
+Version 0.2.1
+-------------
+
+* include README.md in distribution to fix install
+
+Version 0.2.0
+-------------
+
+* python3 updates
+* enforce winding order on multipolygons
+* update key/val handling
+* round floating point values instead of truncating
+* add option to quantize bounds
+* add option to flip y coord system
+* add ability to pass custom extents
+
+Version 0.1.0
+-------------
+
+* Add compatibility with python 3
+* Handle multipolygons as single features
+* Use winding order from mapbox vector tile 2.0 spec
+* Support custom extents when decoding
+
+Version 0.0.11
+--------------
+
+* Decode string keys to utf-8
+
+Version 0.0.10
+--------------
+
+* Allow encoder to accept shapely objects directly
+
+Version 0.0.9
+-------------
+
+* Handle tiles from java-vector-tile (zero pad binary integers)
+* Update README
+
+Version 0.0.8
+-------------
+
+* Handle unicode properties
+
+Version 0.0.7
+-------------
+
+* Update id handling behavior
+
+Version 0.0.6
+-------------
+
+* Explode multipolygons into several features
+* https://github.com/tilezen/mapbox-vector-tile/issues/4
+* Resolve issue when id is passed in
+* More tests
+
+Version 0.0.5
+-------------
+
+* Removing the option of encoding floats in big endian
+* Updated tests
+
+Version 0.0.4
+-------------
+
+* Bug fix - does not try to load wkt geom if wkb succeeds 
+
+Version 0.0.3
+-------------
+
+* Option to encode floats in little endian
+
+Version 0.0.2
+-------------
+
+* WKT Support
+* Better Documentation
+* More tests
+
+Version 0.0.1
+-------------
+
+* Initial release
--- /dev/null
+++ b/LICENSE
@@ -0,0 +1,22 @@
+The MIT License (MIT)
+
+Copyright (c) 2014 Mapzen
+
+Permission is hereby granted, free of charge, to any person obtaining a copy
+of this software and associated documentation files (the "Software"), to deal
+in the Software without restriction, including without limitation the rights
+to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
+copies of the Software, and to permit persons to whom the Software is
+furnished to do so, subject to the following conditions:
+
+The above copyright notice and this permission notice shall be included in all
+copies or substantial portions of the Software.
+
+THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
+IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
+FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
+AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
+LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
+OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
+SOFTWARE.
+
